﻿Objective
=========
The purpose of this script is to interpolate point data to raster surfaces using a minimum curvature spline
technique and with land barriers (polygon or polyline). See link for details on the Spline with Barriers tool:
https://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/spline-with-barriers.htm.

Summary
=======
The script interpolates point data such as mean summer temperature derived from oceanographic models. The Z_value_field
parameter in the Spline with Barriers tool is obtained from the 3rd column (with a python index 2) of the shapefile or
feature class data. Prepare input data prior to running this tool to ensure that the values that should be used for
interpolation are in the 3rd column of the data. The script checks the projections of the input data and will exit if
they are not the same. It will not reproject and of the layers to a common spatial reference system. The script uses
the 'name' property of the spatial reference object to compare the spatial reference systems form different layers. See
https://desktop.arcgis.com/en/arcmap/latest/analyze/arcpy-classes/spatialreference.htm for details on the spatial
reference object.

The land barriers file can be a polygon or a polyline. It is recommended that the barrier be buffered inland some extent
(2 km for example) to ensure that the interpolation extends at least to land.

The base names from the point source data become the names for the output rasters in TIFF format which are exported
to an output directory created one level above where the script is executed. Depending on the extent of the input data
and the interpolation cell size, the spline interpolation may not execute due to memory issues. Therefore, the user may
interpolate to a lower resolution and then resample the interpolated surfaces to a higher resolution.

Status
======
Ongoing-improvements.

Contents
========
**spline-barriers.py**: Standalone script to be run by user in a command line interface such as command prompt.

Methods
=======
The script is meant to be run from a command line interface and expects 6 arguments from the user, detailed below:

**Input barrier file:** Path to vector polygon or polyline file.

**Cell size:** Integer value to use for output cell size for Spline with Barriers interpolation.

**Input snap raster** Path to raster file to set snap environment.

**Input data directory:** Path to input source points directory (Assumes shapefiles in directory).

**Resample data:** Execute a resample after interpolating raster surfaces? Yes/No.

**Resample integer value:** Positive integer value to resample interpolated surfaces to. Enter 0 if not applicable.

Example usage:
c:\\Python27\\ArcGIS10.6\\python.exe spline-barriers.py C:\\Temp\\land_file.shp 50 C:\\Temp\\bathymetry.tif C:\\Temp\\input_data Yes 20

It is recommended that the spline-barriers.py script be places in a scripts folder. The interpolated_raw directory will
be generated one level above the directory where the script is executed from. If resampling is Yes, a resampled
directory will be created as well. See below for directory structure:

├───interpolated_raw
    └───TIFF files interpolated using Spline with Barriers
├───resampled
    └───resampled TIFF files (directory created if resampling is Yes)
└───scripts
    └───spline-barriers.py

Input points are interpolated with the smoothing option set to 1. See
https://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/spline-with-barriers.htm. for details.
Interpolated surfaces are snapped to the snap raster and exported to the interpolated_raw directory. A log file will
be written to the directory with details. If resampling is Yes, then the interpolated surfaces will be resampled to the
resampled directory. A log file will be written to the resampled directory with details.

Requirements
============
* ArcPy module (available with an ArcGIS license).
* Spatial Analyst extension.
* Java runtime environment Version 6 or higher

Caveats
=======
* The Z_value_field parameter in the Spline with Barriers tool is obtained from the 3rd column (with a python index 2)
  of the shapefile or feature class data.
* The user should note any areas of interpolation that extend beyond the source data. For example, inlets where there
  are no source data, but the raster surface has been interpolated.

Uncertainty
===========
* Highly complex geometry in the land barrier file will increase processing time and potentially cause memory issues,
  depending on the machine executing the code.

Acknowledgements
================
Jessica Nephin: DFO Science, Marine Spatial Ecology and Analysis section.

References
==========
* Environmental Systems Research Institute (ESRI). (2019). ArcGIS Release 10.6. Redlands, CA.
