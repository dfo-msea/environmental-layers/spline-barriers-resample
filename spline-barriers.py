#!/usr/bin/env python

"""
Execute spline with barriers. Requires ArcPy modules, spatial analyst extension, Java runtime environment Version 6 or
higher. See here: https://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/spline-with-barriers.htm
Optionally resample interpolated surfaces to a higher-resolution. The reason for resampling rather than interpolating
to a desired resolution is because of memory limitations. Depending on the extent of the source data and the
resolution, the interpolation may fail. Therefore, sometimes interpolation to a lower resolution is required as a first
step and resampling is required to reach a higher resolution.
"""

# Import system modules
import os
import sys
import time
import getpass
import socket
from datetime import date
import arcpy
from arcpy.sa import *

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Usage examples printed to console if user provides arguments when running script.
usage = "<input barrier file (vector land file)> <cell size (number)> <input raster file to set snap environment> " \
        "<input data directory> <resample data (Yes/No)> <resample integer value (Enter 0 if not applicable)>"
usage_example = "python spline-barriers.py C:\\Temp\\land_file.shp 50 C:\\Temp\\bathymetry.tif C:\\Temp\\input_data " \
                "Yes 20"

if len(sys.argv) < 7:
    raise Exception, "Invalid number of parameters provided. \n\n" + "Usage: " + usage + "\n\nExample: " + usage_example

# Input land barrier file.
in_barrier_feature = sys.argv[1]

# Cell size for interpolation.
cell_size = sys.argv[2]

# Raster to use for snapping to.
snap_raster = sys.argv[3]

# Input directory with data to interpolate.
input_directory = sys.argv[4]

# Resample (true or false) - convert to lowercase.
resample_rasters = sys.argv[5].lower()

# Resample value - convert string to integer.
resample_value = int(sys.argv[6])

# Smoothing option for spline with barriers.
smoothing = 1

print("Reading coordinate system of land feature class...")
coord_sys = arcpy.Describe(in_barrier_feature).spatialReference
arcpy.env.outputCoordinateSystem = coord_sys
print("Land feature coordinate reference system:\n{}\n".format(coord_sys.Name))

# Set Snap Raster in environment.
arcpy.env.snapRaster = snap_raster

# Check the coordinate system of the snap raster file. If it is not the same as the land features
# (used to set the environment), discontinue and exit.
print("Reading coordinate system of the snap raster file...")
snap_raster_coord_sys = arcpy.Describe(snap_raster).spatialReference
print("Snap raster coordinate reference system:\n{}\n".format(snap_raster_coord_sys.Name))
if not snap_raster_coord_sys.Name == coord_sys.Name:
    arcpy.AddError("Snap raster and land feature class must have the same projection! Exiting program.")
    sys.exit()

# Set workspace in environment.
arcpy.env.workspace = input_directory

# Get list of shapefiles/feature classes in input directory.
shapefiles = arcpy.ListFeatureClasses()

# Create output directory up one level from where script is executed.
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
os.chdir("..")
out_dir = "interpolated_raw"
root_dir = os.getcwd()
lnbrk = "=============================="

# Execute spline with barriers tool with input data.
try:
    # Start timer for interpolation.
    start = time.time()

    # Check if output directory already exists, if not, create it.
    if not os.path.exists(out_dir):
        out_path = os.path.join(root_dir, out_dir)
        print("Creating output directory here: {}".format(out_path))
        os.mkdir(out_dir)
    else:
        print("{} folder already exists. Exiting program.".format(out_dir))
        sys.exit()

    # Loop through each shapefile/fc and check projections.
    for shp in shapefiles:
        print("Reading coordinate system of {}...".format(shp))
        pnt_coord_sys = arcpy.Describe(shp).spatialReference
        print("{} coordinate reference system:\n{}\n".format(shp, pnt_coord_sys.Name))
        if not pnt_coord_sys.Name == coord_sys.Name:
            arcpy.AddError("Input points and land feature class must have the same projection! Exiting program.")
            sys.exit()

    # Counter variable.
    counter = 1

    # Redirect messages from console to log file.
    old_stdout = sys.stdout
    log_file_path = os.path.join(out_path, "messages-spline.log")
    print("Messages will be redirected from console to log file here: {}.".format(log_file_path))
    log_file = open(log_file_path, "w")
    sys.stdout = log_file

    # Print details about script run to user.
    print("Script executed on: {} by {} on {}.".format(date.today(), getpass.getuser(), socket.gethostname()))
    print(lnbrk)
    print("Input land barrier file: {}".format(in_barrier_feature))
    print("Cell size for interpolation: {}".format(cell_size))
    print("Raster used to snap output to: {}".format(snap_raster))
    print("Input directory for source data: {}".format(input_directory))
    print("Output directory for interpolated data: {}".format(out_path))
    print(lnbrk)

    # Print message for user.
    print("Interpolation: Started")

    # Loop through each shapefile/fc and interpolate points using spline w/barriers.
    for shp in shapefiles:
        print("Working on file {} of {}: {}".format(counter, len(shapefiles), shp))
        # Assign arguments for tool.
        fields = arcpy.ListFields(shp)
        z_field = fields[2].name
        infeature = shp

        # Execute Spline with Barriers
        print("Executing Spline with Barriers...")
        out_spline = SplineWithBarriers(infeature, z_field, in_barrier_feature, cell_size, smoothing)

        # Get name from shapefile/feature class and append .tif to the output raster.
        outname = os.path.splitext(shp)[0] + ".tif"
        outpath = os.path.join(out_dir, outname)

        # Save interpolated surface to disk.
        print("Writing {} to {}...".format(outname, out_dir))
        out_spline.save(outpath)

        # Increment counter for print statements.
        counter += 1

    print(lnbrk)
    print("Interpolation: Completed")
    print(lnbrk)
    end = time.time()
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Interpolation processing time:\n{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

    # Close log file and return stout to default.
    sys.stdout = old_stdout
    log_file.close()

except Exception as e:
    print(e.message, e.args)

# If resample option is selected, resample interpolated surfaces to desired resolution.
if resample_rasters == "yes":
    try:
        # Start time for resampling.
        start = time.time()

        # Resampled output directory.
        out_dir_resample = "resampled"

        # Check if resampled output directory already exists, if not, create it.
        if not os.path.exists(os.path.join(root_dir, out_dir_resample)):
            out_path = os.path.join(root_dir, out_dir_resample)
            print("Creating output directory here: {}".format(out_path))
            os.mkdir(out_dir_resample)
        else:
            # If output directory for resampling already exists, exit program.
            print("{} folder already exists. Exiting program.".format(out_dir_resample))
            sys.exit()

        # Redirect messages from console to log file.
        old_stdout = sys.stdout
        log_file_path = os.path.join(out_path, "messages-spline.log")
        print("Messages will be redirected from console to log file here: {}.".format(log_file_path))
        log_file = open(log_file_path, "w")
        sys.stdout = log_file

        # Print details about script run to user.
        print("Script executed on: {} by {} on {}.".format(date.today(), getpass.getuser(), socket.gethostname()))
        print(lnbrk)
        print("Input directory for source data (interpolated rasters): {}".format(os.path.join(root_dir, out_dir)))
        print("Output directory for resampled rasters: {}".format(out_path))
        print("Original interpolated raster cell size: {}".format(cell_size))
        print("Resampled raster cell size: {}".format(resample_value))
        print(lnbrk)

        # Set ArcGIS workspace to old out directory.
        arcpy.env.workspace = os.path.join(root_dir, out_dir)

        # Create list of rasters in workspace where interpolated surfaces live.
        raster_list = arcpy.ListRasters()

        # Reset counter variable.
        counter = 1
        print("Resampling: Started")

        # Iterate rasters and resample to desired resolution.
        for raster_layer in raster_list:
            print("Working on file {} of {}".format(counter, len(raster_list)))

            # Get filename from path.
            out_file = os.path.join(out_path, raster_layer)
            print("Resampling {} to {} in units of coordinate reference system. "
                  "Exporting to {}...".format(raster_layer, resample_value, out_path))

            # Resample raster to specified resolution.
            resample = arcpy.Resample_management(raster_layer,
                                                 out_file,
                                                 "{} {}".format(resample_value, resample_value),
                                                 "BILINEAR")

            # Increment counter.
            counter += 1

        # Print message to user that resampling is complete.
        print(lnbrk)
        print("Resampling: Completed")
        print(lnbrk)
        end = time.time()
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        print("Resampling processing time:\n{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

        # Close log file and return stout to default.
        sys.stdout = old_stdout
        log_file.close()

    except Exception as e:
        print(e.message, e.args)
else:
    # Exit program.
    print("Resampling procedure not selected. Exiting program.")
    sys.exit()













